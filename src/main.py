import asyncio
import logging
import sys

from ariadne import graphql_sync
from ariadne.constants import PLAYGROUND_HTML
from flask import Flask, request, jsonify

from schema import get_schema


app = Flask(__name__)
schema = get_schema()


@app.route("/graphql", methods=["GET"])
async def graphql_playground():
    # Being an example app this is nice and convenient
    return PLAYGROUND_HTML, 200


@app.route("/graphql", methods=["POST"])
async def graphql_server():
    # GraphQL queries are always sent as POST
    data = request.get_json()

    # Note: Passing the request to the context is optional.
    # In Flask, the current request is always accessible as flask.request
    logging.getLogger().debug(data)
    success, result = graphql_sync(
        schema,
        data,
        context_value=request,
        debug=app.debug
    )

    status_code = 200 if success else 400
    return jsonify(result), status_code


if __name__ == "__main__":
    mode = sys.argv[1] if len(sys.argv) >= 2 else "development"
    debug = (mode == "development")
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG if debug else logging.ERROR)

    app.run(debug=debug, host='0.0.0.0', port=80)
