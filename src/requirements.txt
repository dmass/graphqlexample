aniso8601==9.0.1
anyio==3.4.0
ariadne==0.14.0
asgiref==3.4.1
attrs==21.2.0
click==8.0.3
Flask==2.0.2
graphene==3.0
graphql-core==3.1.6
graphql-relay==3.1.0
idna==3.3
iniconfig==1.1.1
itsdangerous==2.0.1
Jinja2==3.0.3
MarkupSafe==2.0.1
packaging==21.3
pluggy==1.0.0
py==1.11.0
pyparsing==3.0.6
pytest==6.2.5
sniffio==1.2.0
starlette==0.17.1
toml==0.10.2
typing-extensions==4.0.1
Werkzeug==2.0.2
