from os import path
from pathlib import Path
from ariadne import load_schema_from_path, make_executable_schema, ObjectType
import shutil

from .data import get_data_path
from .item import item
from .query import query, server_info
from .mutation import mutation


def get_schema():
    schema_path = path.join(path.join(Path(__file__).resolve().parent, "schema.graphql"))
    schema_str = load_schema_from_path(schema_path)
    return make_executable_schema(schema_str, query, server_info, item, mutation)


shutil.copyfile(path.join(Path(__file__).resolve().parent, 'data.json'), get_data_path())
