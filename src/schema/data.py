from pathlib import Path
from os import path


def get_data_path():
    return path.join(Path(__file__).resolve().parent, '_data.json')

