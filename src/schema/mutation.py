import json
from os import path
from pathlib import Path
from ariadne import MutationType

from schema.data import get_data_path

mutation = MutationType()


@mutation.field("update_item")
def update_item(obj, info, changes):
    with open(get_data_path(), "r") as f:
        data = json.load(f)
    name = changes["name"]
    found = False
    for item in data["items"]:
        if item["name"] == name:
            item.update(changes)
            found = True
            break

    if not found:
        data["items"].append(changes)

    with open(get_data_path(), "w") as f:
        json.dump(data, f, indent=2)
    return True
