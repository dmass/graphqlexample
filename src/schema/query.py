import json
from ariadne import QueryType, ObjectType

from schema.data import get_data_path

query = QueryType()


@query.field("hello")
def hello(obj, info):
    # using request context as given by 'context_value' kwarg in e.g. 'graphql_sync'
    request = info.context
    user_agent = request.headers.get("User-Agent", "Guest")
    return "Hello, %s!" % user_agent


@query.field("echo")
def echo(_, info, **kwargs):
    # using parameters
    return kwargs["message"]


@query.field("info")
def get_info(obj, info, **kwargs):
    # returning another object type
    description = "This is an example implementation for a GraphQL backend"
    return {
        "description": description,
        "description_overridden": description,
    }


@query.field("search")
def search(obj, info, text):
    with open(get_data_path()) as data_file:
        data = json.load(data_file)
    items = data["items"]
    return list(filter(lambda o: text in o["name"], items))


server_info = ObjectType("ServerInfo")


@server_info.field("description_overridden")
def info_description(obj, info):
    return "Without the extra resolver this value would be the same as the description field"
