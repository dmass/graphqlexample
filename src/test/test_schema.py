import json
import pytest
from ariadne import graphql_sync

from schema import get_schema


class TestSchema:
    def test_trivial(self):
        assert True

    def test_query(self):
        data = {
            'operationName': None,
            'variables': {},
            'query': """{ 
                search(text: "ndy") {
                    name  
                    price
                }
            }"""
        }
        success, result = graphql_sync(
            get_schema(),
            data,
            # context_value=request,
            debug=True
        )
        assert result["data"]["search"][0]["name"] == "Candy Bar"
