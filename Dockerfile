FROM ubuntu:latest

ARG install_dir=/opt/graphql_example/
ENV install_dir ${install_dir}
ENV main_path ${install_dir}main.py
ENV mode development

ADD src ${install_dir}
RUN apt-get update && \
apt-get install python3-pip -y && \
pip install -r ${install_dir}requirements.txt

EXPOSE 80
CMD ["/bin/bash", "-c", "cd ${install_dir} && python3 main.py ${mode}"]